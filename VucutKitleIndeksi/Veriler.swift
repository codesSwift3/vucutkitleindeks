//
//  Veriler.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 1.10.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit

class Veriler: UITableViewCell {

    @IBOutlet weak var lbKilo: UILabel!
    @IBOutlet weak var lbVki: UILabel!
    @IBOutlet weak var lbTarih: UILabel!
    @IBOutlet weak var lbDurum: UILabel!
    @IBOutlet weak var lbHedefKiloDurumu: UILabel!
    @IBOutlet weak var lbhedefKilo: UILabel!
    
    
    
    //@IBOutlet weak var tvCumle: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func confic(kilo:String, vki:String){
        self.lbKilo.text  = kilo
        self.lbVki.text   = vki
        
       
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
