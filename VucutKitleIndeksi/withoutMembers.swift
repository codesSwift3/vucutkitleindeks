//
//  withoutMembers.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 26.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit

class withoutMembers: UIViewController {

    @IBOutlet weak var switch1: UISwitch!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var tfBoy: UITextField!
    @IBOutlet weak var tfKilo: UITextField!
    @IBOutlet weak var lbDurum: UILabel!
    @IBAction func btnSonucuGoster(_ sender: Any) {
        // Boy ve kilo boş geçilemeceğini kontrol ediyorum.
        if(tfBoy.text=="" || tfKilo.text=="")
        {
            let uyari = UIAlertController(title: "title", message: "bir bir alert", preferredStyle:UIAlertControllerStyle.alert)
            uyari.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: { (action) in}))
            uyari.dismiss(animated: true, completion:nil)
            self.present(uyari, animated: true, completion: nil)
        }
        else
        {   // Kadınlar için.
            if(switch1.isOn)
            {
                let boy = Float(self.tfBoy.text!)
                let kilo = Float(self.tfKilo.text!)
                let name = self.tfName.text
                lbName.text = ("Merhaba \(String(describing: name!))")
                let vki = self.sonuc(kilo: kilo!, boy: boy!)
                lbSonuc.text = String(vki)
                
                if(vki<18.5)                    {lbDurum.text = "Zayıf"}
                else if(vki>=17.5 || vki<=23.9) {lbDurum.text="Normal"}
                else if(vki>=24   || vki<=28.9) {lbDurum.text="Kilolu"}
                else if(vki>=29)                {lbDurum.text="Obez"}
                
            }
            else
                
            {// Erkekler için.
                let boy = Float(self.tfBoy.text!)
                let kilo = Float(self.tfKilo.text!)
                let name = self.tfName.text
                lbName.text = ("Merhaba \(String(describing: name!))")
                let vki = self.sonuc(kilo: kilo!, boy: boy!)
                lbSonuc.text = String(vki)
               
                if(vki<18.5)                    {lbDurum.text = "Zayıf"}
                else if(vki>=18.5 || vki<=24.9) {lbDurum.text="Normal"}
                else if(vki>=25   || vki<=29.9) {lbDurum.text="Kilolu"}
                else if(vki>=30)                {lbDurum.text="Obez"}
            }
        }

    }
    func sonuc(kilo:Float,boy:Float)->Float
    {
        let vki = (kilo/(boy*boy)*10000)
        return vki
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var lbSonuc: UILabel!

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
