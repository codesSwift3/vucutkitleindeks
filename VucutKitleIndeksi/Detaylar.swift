//
//  Detaylar.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 10.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit
import Firebase
class Detaylar: UIViewController,UITableViewDataSource,UITableViewDelegate{
    let ref = Database.database().reference()
    let user = Auth.auth().currentUser?.uid
   
    var userInfo      = [String]()
    var tarihler      = [String]()
    var kilolar       = [String]()
    var vkiler        = [String]()
    var durumlar      = [String]()
    var hedefKiloDizi = [String]()
    var al            = [Int]()
    var ver           = [Int]()
    
    
    
    @IBOutlet weak var tvMotiveCumle: UITextView!
    //  var cumleler = [String]()
    @IBOutlet weak var verilerTableView: UITableView!
    override func viewDidLoad() {
        
        
        // Kullanıcı verilerini alıyorum.
        ref.child("users").child(user!).observeSingleEvent(of: .value, with: { (snapshot) in
           // key = value şeklinde olduğu için NSDictionary
            let userValue = snapshot.value as? NSDictionary
            let isim     = userValue?["adSoyad"] as! String
            let kullaniciAdi = userValue?["kullaniciAdi"] as! String
            let motiveCumle = userValue?["motiveCumle"] as! String
            let hedefKilo = userValue?["hedeflenenKilo"] as! String
            
            self.tvMotiveCumle.text = motiveCumle
                
           
            // Verileri oluşturduğum dizilere ekliyorum
            print("Kullanıcı Adi :\(kullaniciAdi)")
            self.userInfo.append(isim)
            self.userInfo.append(kullaniciAdi)
            
           //self.cumleler.append(motiveCumle)
            
          
            // Kullanıcı verilerini alıyorum.
            self.ref.child("veriler/\(self.user!)").observe(.childAdded, with: { (snapshot) in
                let value = snapshot.value as? NSDictionary
                let kilo  = value?["Kilo"] as! String
                let tarih = value?["Tarih"] as! String
                let vki   = value?["Vki"] as! String
                let boy   = value?["Boy"] as! String
                let durum   = value?["Durum"] as! String
                
                

                
                
                print("KİLO---> \(kilo)")
                print("VKI----> \(vki)")
                print("BOY---> \(boy)")
                print("HEDEFLENEN KİLO----> \(hedefKilo)")

                //Verileri diziye atıyorum.
                self.kilolar.append(kilo)
                self.vkiler.append(vki)
                self.tarihler.append(tarih)
                self.durumlar.append(durum)
                self.hedefKiloDizi.append(hedefKilo)
                
                let hedefKiloInt:Int = Int(self.hedefKiloDizi[0])!
                let sonKilo = self.kilolar.count - 1
                let kiloInt:Int = Int(self.kilolar[sonKilo])!
                let kiloVer = kiloInt-hedefKiloInt
                self.ver.append(kiloVer)
                let kiloAl  = hedefKiloInt-kiloInt
                self.al.append(kiloAl)
                
                
                
               
                
                
                // Firebaseden gelen verileri satır satır tableView'e yerleştiriyorum
               self.verilerTableView.insertRows(at: [IndexPath(row:self.tarihler.count-1,section:0)], with: UITableViewRowAnimation.automatic)
                
            })
            
        }) { (error) in
            
            print(error.localizedDescription)
        }
        
        
        
    }
    //Atılan her tarih kadar satır oluşacak.
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return tarihler.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
       // TableView'i dolduruyorum.
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! Veriler
        let kiloCount = kilolar.count
        let vkiCount  = vkiler.count
        let tarihCount = tarihler.count
      // let tcumleler = cumleler.count
        cell.lbKilo.text = kilolar[kiloCount-1]
        cell.lbVki.text = vkiler[vkiCount-1]
        cell.lbTarih.text = tarihler[tarihCount-1]
        cell.lbDurum.text = durumlar[0]
        cell.lbhedefKilo.text = hedefKiloDizi[0]
        
        let hedefKiloInt:Int = Int(self.hedefKiloDizi[0])!
        let sonKilo = self.kilolar.count - 1
        
        if(sonKilo > hedefKiloInt){
            let stringver:String = String(ver[0])
            cell.lbHedefKiloDurumu.text = stringver
        }
        else if(sonKilo < hedefKiloInt){
            let stringal:String = String(al[0])

            cell.lbHedefKiloDurumu.text = stringal
            
        }

        
        
        
      //cell.tvCumle.text = cumleler[0]
        
        
        return cell
    }
    // Yönlendirme butonları
    @IBAction func geriGit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let oneriler = storyBoard.instantiateViewController(withIdentifier: "oneriler") as! Oneriler
        self.present(oneriler, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
