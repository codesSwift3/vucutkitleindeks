//
//  animasyon.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 15.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//
import UIKit

class animasyon: UIButton{
    func titret(){
        let animasyon = CABasicAnimation(keyPath: "position") // Animasyon pozisyon değişimiyle gerçekleşecek
        animasyon.duration = 0.07 // Animasyon süresi
        animasyon.repeatCount = 4 // Tekrar
        animasyon.autoreverses = true // Eski Pozsiyonuna dönecek
        animasyon.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 6, y: self.center.y)) // X ekseninde hareketi
        animasyon.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 6, y: self.center.y)) // Y ekseninde hareketi
        self.layer.add(animasyon, forKey: "position") // ve Layer'e ekliyorum
    }
}
